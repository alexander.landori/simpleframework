### start app
```
docker-composer up -d
```

### composer
```
docker-compose exec php-fpm composer dump-autoload
docker-compose exec php-fpm composer install
```
