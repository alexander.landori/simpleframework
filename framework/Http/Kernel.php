<?php
namespace Link0\Framework\Http;

use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;
use Link0\Framework\Http\Response;

class Kernel
{
    public function handle(Request $request): Response {
        $dispatcher = simpleDispatcher(function(RouteCollector $routeCollector){
            $routes = include BASE_PATH . '/routes/web.php';

            foreach($routes as $route){
                $routeCollector->addRoute(...$route);
            }
        });

        $routeInfo = $dispatcher->dispatch($request->getMethod(), $request->getPathInfo());

        [$status, $handler, $variables] = $routeInfo;

        if( $handler instanceof \Closure ){
            return $handler($variables);
        } else if( is_array($handler) ){
            [$controller, $method] = $handler;

            return call_user_func_array([new $controller, $method], $variables);
        } else {
            return new Response('Unknown handler', 500);
        }
    }
}