<?php
namespace Link0\Framework\Http;

class Response
{
    public function __construct(
        private ?string $content = '',
        private int $status = 200,
        private array $headers = [],
    ) {
        //
    }

    public function send(): static {
        $this->sendHeaders();

        echo $this->content;

        if (\function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        }

        return $this;
    }

    private function sendHeaders(): static {
        if( headers_sent() ){
            return $this;
        }

        if( !empty($this->headers) ){
            foreach($this->headers as $header){
                header($header, true, $this->status);
            }
        }

        http_response_code($this->status);

        return $this;
    }
}