<?php
declare(strict_types=1);

use Link0\Framework\Http\Kernel;
use Link0\Framework\Http\Request;
use Link0\Framework\Http\Response;

define('BASE_PATH', dirname(__DIR__));

require_once dirname(__DIR__) . '/vendor/autoload.php';

$request = Request::createFromGlobals();

$kernel = new Kernel();

$response = $kernel->handle($request);

$response->send();
