<?php
namespace App\Controllers;

use Link0\Framework\Http\Response;

class HomeController
{
    public function index()
    {
        return new Response('Home page');
    }
}