<?php
namespace App\Controllers;

use Link0\Framework\Http\Response;

class PostsController
{
    public function show(int $id): Response {
        return new Response("Show post {$id}");
    }
}